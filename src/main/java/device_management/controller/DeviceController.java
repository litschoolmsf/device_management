package device_management.controller;

import device_management.config.ErrorType;
import device_management.entity.Contact;
import device_management.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping({"/device"})
public class DeviceController extends ApiBaseController {
    @Autowired
    public ContactRepository contactRepository;

    @GetMapping({"/list"})
    public ResponseEntity list() {
        try {
            return responseOK("");
        } catch (Exception ex) {
            ex.printStackTrace();
            return responseError(ErrorType.TRY_CATCH, ex.getMessage());
        }
    }

    @GetMapping({"/add"})
    public ResponseEntity<Object> add() {
        Contact contact = new Contact();
        contact.set_id(2);
        contact.setAge(18);
        contact.setName("test");
        contact.setEmail("email test");
        contactRepository.delete(contact);
        return responseOK("");
    }
}

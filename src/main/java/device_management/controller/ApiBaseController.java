package device_management.controller;

import device_management.config.ApiResponseEntity;
import device_management.config.ErrorType;
import device_management.config.ResponseError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin("*")
public class ApiBaseController {
    @Autowired
    public HttpServletRequest request;

    protected final ResponseEntity<Object> responseOK(Object data) {
        return ResponseEntity.ok(new ApiResponseEntity(ResponseError.ErrorResponseEntity.ok(), data));
    }

    protected final ResponseEntity<Object> responseError(
            ErrorType errorType , String message) {
        return ResponseEntity.ok(
                new ApiResponseEntity(
                        ResponseError.ErrorResponseEntity.error(errorType, message)
                ).toResponse()
        );
    }
}

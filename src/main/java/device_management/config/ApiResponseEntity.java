package device_management.config;

import java.util.HashMap;

public class ApiResponseEntity {
    public ResponseError.ErrorResponseEntity resultState;
    private Object data;

    public ApiResponseEntity(ResponseError.ErrorResponseEntity error) {
        this.resultState = new ResponseError.ErrorResponseEntity(error.getCode(), error.getMessage());
    }

    public ResponseError.ErrorResponseEntity getResultState() {
        return this.resultState;
    }

    public void setResultState( ResponseError.ErrorResponseEntity var1) {
        this.resultState = var1;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object var1) {
        this.data = var1;
    }

    public final Object toResponse() {
        HashMap result = new HashMap();
        HashMap objState = new HashMap();
        ResponseError.ErrorResponseEntity var10002 = this.resultState;

        objState.put("code", var10002.getCode());
        var10002 = this.resultState;


        objState.put("message", var10002.getMessage());
        result.put("status", objState);
        if (this.data != null) {
            Object var3 = this.data;
            result.put("data", var3);
        }

        return result;
    }


    public ApiResponseEntity(ResponseError.ErrorResponseEntity errorEntity, Object data) {
        this.resultState = new ResponseError.ErrorResponseEntity(errorEntity.getCode(), errorEntity.getMessage());
        this.data = data;
    }
}

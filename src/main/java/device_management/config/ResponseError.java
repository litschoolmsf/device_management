package device_management.config;

import java.util.HashMap;

public class ResponseError {

    public static class ErrorResponseEntity {
        private int code = 0;
        private String message = "";

        public ErrorResponseEntity() { }

        public ErrorResponseEntity(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

//        int getCode() { return this.code; }
//        void setCode(int code){this.code = code; }
//        String getMessage() { return this.message; }
//        void setMessage(String message) {this.message = message;}

        public static ErrorResponseEntity error(ErrorType errorCode, String message) {
            String error_message = message;
            if (error_message == null || error_message == "") {
                error_message = new ErrorMessage(errorCode.getCode()).message();
            }
            return new ErrorResponseEntity(errorCode.getCode(), error_message);
        }

        public static ErrorResponseEntity ok() {
            return new ErrorResponseEntity(100, new ErrorMessage(100).message());
        }
    }

    static class ErrorMessage{
        private int error_code = 0;
        public ErrorMessage () {}

        public int getError_code() { return error_code; }

        public void setError_code(int error_code) { this.error_code = error_code; }

        public ErrorMessage (int error_code) { this.error_code = error_code; }

        String message() {
            HashMap messageMap = new HashMap<Integer, String>();
            messageMap.put(ErrorType.ERROR_HEADER.getCode(), "header param error");
            messageMap.put(ErrorType.ERROR_REQUEST.getCode(), "data post error");
            messageMap.put(ErrorType.OK.getCode(), "Response OK");
            messageMap.put(ErrorType.TRY_CATCH.getCode(), "Exception error");
            if (messageMap.containsKey(error_code)){
                return messageMap.get(error_code).toString();
            } else {
                return  "";
            }

        }
    }



}



package device_management.config;

public enum ErrorType {
    OK(100),
    ERROR_HEADER(101),
    ERROR_REQUEST(102),
    TRY_CATCH(103);

    private int code;
    ErrorType(int code) {
        this.code = code;
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
}

package device_management.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Contact")
public final class Contact {
    @Id
    private int _id;
    private String name = "";
    private int age;
    private String email = "";

    public int get_id() {
        return this._id;
    }

    public void set_id(int var1) {
        this._id = var1;
    }

    public String getName() {
        return this.name;
    }

    public void setName( String var1) { this.name = var1; }

    public int getAge() {
        return this.age;
    }

    public void setAge(int var1) {
        this.age = var1;
    }

    public String getEmail() { return this.email; }

    public void setEmail(String var1) { this.email = var1; }
}
